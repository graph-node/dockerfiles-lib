
# Install dependencies only when needed
FROM node:14-alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat git
WORKDIR /app
COPY .yarn/ ./.yarn
COPY package.json .yarnrc.yml yarn.lock ./
RUN yarn install --immutable

# Rebuild the source code only when needed
FROM deps AS builder

WORKDIR /app
ENV NODE_ENV=production

COPY . .
RUN yarn build && yarn workspaces focus --all --production && rm -rf .next/cache

# Production image, copy all the files and run next
FROM node:14-alpine AS pipeline
WORKDIR /app

ENV NODE_ENV production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder --chown=nextjs:nodejs /app/.next ./.next
COPY --from=builder /app/.yarn/ ./.yarn/
COPY --from=builder /app/.pnp.cjs ./.pnp.cjs
COPY --from=builder /app/.yarnrc.yml ./.yarnrc.yml

COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/yarn.lock ./yarn.lock

USER nextjs

EXPOSE 3000

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry.
# ENV NEXT_TELEMETRY_DISABLED 1

CMD ["yarn", "start"]
