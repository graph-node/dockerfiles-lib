
FROM bitwalker/alpine-elixir-phoenix:latest as base

WORKDIR /app

EXPOSE 4000
# Set exposed ports
ENV MIX_ENV=prod

# Cache elixir deps
ADD mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

COPY . .

# Same with npm deps
ADD assets/package.json assets/
RUN cd assets && \
    npm rebuild node-sass && \
    npm install

# Run frontend build, compile, and digest assets
RUN cd assets/ && \
    npm run deploy && \
    cd - && \
    mix phx.digest && \
    mix release --overwrite


RUN ["chmod", "+x", "./entrypoint.sh"]
CMD [ "./entrypoint.sh"]
