FROM node:10 as base

WORKDIR  /usr/app

EXPOSE 3000

COPY package.json ts*.json yarn.lock ./
RUN yarn --quiet --unsafe-perm --no-progress --no-audit --only=production

FROM build as production

COPY . .

RUN yarn run build

CMD [ "node", "dist/src/index" ]

FROM build as development
ENV NODE_ENV=development
RUN yarn --quiet --unsafe-perm --no-progress --no-audit --only=development
# Use volume as . 
CMD [ "yarn", "start" ]
