FROM node:14-alpine as deps

WORKDIR /app


COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile --non-interactive

FROM node:14-alpine as builder

WORKDIR /app

COPY --from=deps /app/node_modules ./node_modules

COPY . .

RUN yarn build && yarn install --frozen-lockfile --production

FROM builder as migration

WORKDIR /app

RUN apk add dumb-init

COPY --from=deps /app/node_modules ./node_modules

USER node

CMD ["dumb-init", "yarn", "database:jobs"]

FROM node:14-alpine as app

WORKDIR /app

RUN apk add dumb-init

COPY --from=builder /app/package.json .
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder --chown=node:node /app/dist ./dist

USER node

EXPOSE 3000

ENV PORT=3000

CMD ["dumb-init", "node", "dist/src/main"]
